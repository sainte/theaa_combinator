# Combinator #

## Enter data separated by a comma, see the magic going on. 🎩 ##

![Screen Shot 2016-11-15 at 12.53.05.png](https://bitbucket.org/repo/BreqrK/images/2252820106-Screen%20Shot%202016-11-15%20at%2012.53.05.png)


### TODO ###

* Add unit tests
* Add other forms of combination / permutation
* Improve UI