//
//  InputViewModel.swift
//  Combination
//
//  Created by Tiago Bencardino on 15/11/2016.
//  Copyright © 2016 AA. All rights reserved.
//

import UIKit

class InputViewModel: NSObject {

    
    func combine(text: String) {
        
        let entries = text.components(separatedBy: ",")
        let combinator = Combinator(entries: entries)
        
        let results = combinator.combineAll()
        showResults(results: results)
    }
    
    func showResults(results: [[String]]) {
     
        let count = String(results.count)
        let resultsString = results.map { "(\($0.joined(separator: "")))" }
        let text = resultsString.joined(separator: ",\n")
        
        let combinatorResult = ["count" : count,
                                "text" : text] as [String : String]
        NotificationCenter.default.post(name: Constants.combinatorDidChanged, object: nil, userInfo: combinatorResult)
    }
}
