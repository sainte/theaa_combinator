//
//  InputViewController.swift
//  Combination
//
//  Created by Tiago Bencardino on 15/11/2016.
//  Copyright © 2016 AA. All rights reserved.
//

import UIKit

class InputViewController: UIViewController {

    @IBOutlet weak var entriesTextField: UITextField!
    @IBOutlet weak var resultsLabel: UILabel!
    @IBOutlet weak var resultsTextView: UITextView!
    
    let viewModel = InputViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addObservers()
    }
    
    deinit {
        
        removeObservers()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        textField.resignFirstResponder()
        
        if let text = textField.text {
            viewModel.combine(text: text)
        }
        
        return true
    }
    
    func addObservers() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(InputViewController.show(notification:)), name: Constants.combinatorDidChanged, object: nil)
    }
    
    func removeObservers() {
        
        NotificationCenter.default.removeObserver(self, name: Constants.combinatorDidChanged, object: nil)
    }
    
    @objc func show(notification: Notification) {
        
        let result = notification.userInfo
        
        self.resultsTextView.text = result?["text"] as! String!
        let count = result?["count"]
        self.resultsLabel.text = "Results: \(count!)"
    }
}
