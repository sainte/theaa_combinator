//
//  Constants.swift
//  Combination
//
//  Created by Tiago Bencardino on 15/11/2016.
//  Copyright © 2016 AA. All rights reserved.
//

import Foundation

struct Constants {
    static let combinatorDidChanged = NSNotification.Name("combinatorDidChanged")
}
