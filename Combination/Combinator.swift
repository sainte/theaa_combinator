//
//  Combinator.swift
//  Combination
//
//  Created by Tiago Bencardino on 15/11/2016.
//  Copyright © 2016 AA. All rights reserved.
//

import UIKit

class Combinator: NSObject {

    var entries = [String]()
    var answers = [[String]]()
    
    init(entries: [String]) {
        
        self.entries = entries
    }
    
    /** This function call combinations method for every single entry.
    * @return a matrix with all possible combinations
    */
    func combineAll() -> [[String]] {
        
        self.answers = [[String]]()
        
        for i in 0...entries.count {
            var empty = [String](repeating: "", count: i)
            combinations(length: i, startPostion: 0, result: &empty)
        }
        
        return answers
    }
    
    /** This function uses a recursive algorithm to combine all entries, in a non-repeated / non-order way.
    * @param length it's the remaining lenght of operational array
    * @param startPosition incremented cursor on operational array
    * @param result it's the operational array
    */
    func combinations(length : Int, startPostion : Int, result : inout [String]) {
        
        if (length == 0) {
            answers.append(result)
            return
        }
        
        for i in startPostion...entries.count - length {
            
            result[result.count - length] = entries[i]
            combinations(length: length - 1, startPostion: i + 1, result: &result)
        }
    }
}

